package com.guli.common.constants;

/**
 * @author weiyi
 * @describe
 * @since 2019/8/2 - 20:18
 */
public class ResultMessage {

    public static final String DELETE_SECCESS = "DELETE_SECCESS";
    public static final String DELETE_FAILED = "DELETE_FAILED";
    public static final String NO_DATA_QUERIED = "NO_DATA_QUERIED";
    public static final String SAVE_SUCCESS = "SAVE_SUCCESS";
    public static final String SAVE_FAILED = "SAVE_FAILED";
    public static final String UPDATE_SUCCESS = "UPDATE_SUCCESS";
    public static final String UPDATE_FAILED = "UPDATE_FAILED";
    public static final String BAD_SQL_GRAMMAR = "BAD_SQL_GRAMMAR";
    public static final String QUERAY_SUCCESS = "QUERAY_SUCCESS";
    public static final String NOT_QUERAY = "NOT_QUERAY";
    public static final String DATA_IMPORT_SUCCESS = "DATA_IMPORT_SUCCESS";
    public static final String DATA_IMPORT_FAILED = "DATA_IMPORT_FAILED";
}
